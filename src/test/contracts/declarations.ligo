// Test PascaLIGO top-level declarations

const foo : int = 42

function main (const i : int) : int is i + foo
